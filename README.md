### WAVE Testbed CAD and part drawings

The Testbed was modeled using Solidworks 2022

To access the full model, follow these steps:
1. Download the Testbed2pt0.zip 
2. Extract its contents in a desired directory
3. Access the full assembly through Testbed2pt0.SLDASM
